import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SentinelAuthGuardWithLogin, SentinelNegativeAuthGuard } from '@sentinel/auth/guards';
import { SentinelAuthProviderListComponent } from '@sentinel/auth/components';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./visualizations/home-page/home-page.module').then((m) => m.HomePageModule),
    canActivate: [SentinelAuthGuardWithLogin],
  },
  {
    path: 'reference-graph',
    loadChildren: () =>
      import('./visualizations/reference-graph-page/reference-graph-page.module').then(
        (m) => m.ReferenceGraphPageModule,
      ),
    canActivate: [SentinelAuthGuardWithLogin],
  },
  {
    path: 'trainee-graph',
    loadChildren: () =>
      import('./visualizations/trainee-graph-page/trainee-graph-page.module').then((m) => m.TraineeGraphPageModule),
    canActivate: [SentinelAuthGuardWithLogin],
  },
  {
    path: 'mistake',
    loadChildren: () => import('./visualizations/mistake-page/mistake-page.module').then((m) => m.MistakePageModule),
    canActivate: [SentinelAuthGuardWithLogin],
  },
  {
    path: 'timeline',
    loadChildren: () => import('./visualizations/timeline-page/timeline-page.module').then((m) => m.TimelinePageModule),
    canActivate: [SentinelAuthGuardWithLogin],
  },
  {
    path: 'summary-graph',
    loadChildren: () =>
      import('./visualizations/summary-graph-page/summary-graph-page.module').then((m) => m.SummaryGraphPageModule),
    canActivate: [SentinelAuthGuardWithLogin],
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  {
    path: 'login',
    component: SentinelAuthProviderListComponent,
    canActivate: [SentinelNegativeAuthGuard],
  },
  {
    path: '**',
    redirectTo: 'home',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}

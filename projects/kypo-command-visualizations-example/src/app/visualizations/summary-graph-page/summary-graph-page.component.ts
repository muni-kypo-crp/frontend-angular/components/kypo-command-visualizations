import { Component } from '@angular/core';

@Component({
  selector: 'app-summary-graph-page',
  templateUrl: './summary-graph-page.component.html',
  styleUrls: ['./summary-graph-page.component.css'],
})
export class SummaryGraphPageComponent {}

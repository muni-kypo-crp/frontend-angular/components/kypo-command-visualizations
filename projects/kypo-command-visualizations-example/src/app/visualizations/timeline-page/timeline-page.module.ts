import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TimelinePageRoutingModule } from './timeline-page-routing.module';
import { TimelinePageComponent } from './timeline-page.component';
import { TimelineModule } from '@muni-kypo-crp/command-visualizations/timeline';
import { environment } from '../../../environments/environment';

@NgModule({
  declarations: [TimelinePageComponent],
  imports: [CommonModule, TimelinePageRoutingModule, TimelineModule.forRoot(environment.commandVisualizationConfig)],
})
export class TimelinePageModule {}

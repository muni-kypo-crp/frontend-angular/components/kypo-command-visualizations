import { Component } from '@angular/core';

@Component({
  selector: 'app-reference-graph-page',
  templateUrl: './reference-graph-page.component.html',
  styleUrls: ['./reference-graph-page.component.css'],
})
export class ReferenceGraphPageComponent {}

import { NgModule } from '@angular/core';
import { ReferenceGraphPageComponent } from './reference-graph-page.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: ReferenceGraphPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReferenceGraphPageRoutingModule {}

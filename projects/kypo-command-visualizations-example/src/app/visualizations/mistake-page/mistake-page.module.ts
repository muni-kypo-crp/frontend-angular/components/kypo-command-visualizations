import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MistakePageRoutingModule } from './mistake-page-routing.module';
import { MistakeModule } from '@muni-kypo-crp/command-visualizations/mistake';
import { environment } from '../../../environments/environment';
import { MistakePageComponent } from './mistake-page.component';

@NgModule({
  declarations: [MistakePageComponent],
  imports: [CommonModule, MistakePageRoutingModule, MistakeModule.forRoot(environment.commandVisualizationConfig)],
})
export class MistakePageModule {}

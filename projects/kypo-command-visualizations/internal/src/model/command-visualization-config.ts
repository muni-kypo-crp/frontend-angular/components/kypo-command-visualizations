/**
 * Configuration class of command visualizations
 */
export class CommandVisualizationConfig {
  trainingBasePath: string;
  adaptiveBasePath: string;
}

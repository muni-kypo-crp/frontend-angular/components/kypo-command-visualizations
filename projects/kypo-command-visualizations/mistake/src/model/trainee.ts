export class Trainee {
  userRefId: number;
  sub: string;
  fullName: string;
  givenName: string;
  familyName: string;
  iss: string;
}

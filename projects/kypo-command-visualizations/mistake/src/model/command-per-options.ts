export class CommandPerOptions {
  cmd: string;
  commandType: string;
  options: string;
  mistake: string;
  fromHostIp: string;
  frequency: number;
}

export class Command {
  timestamp: string;
  trainingTime: string;
  fromHostIp: string;
  options: string;
  commandType: string;
  cmd: string;
  isForbidden?: boolean;
}
